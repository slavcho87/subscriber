FROM maven:3-jdk-8

VOLUME /tmp
COPY /target/subscriber.jar subscriber.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/subscriber.jar"]
