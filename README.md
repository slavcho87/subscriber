# Adidas Code Challenge - Subscriber

## Database: H2 

As this is a programming test, H2 was chosen as the database because it is a database that is raised in memory when the server is started. This simplifies the configurations of these. For real environments another database should be chosen such as postgresql, mysql depending on the need of the project.
## Environment installation

A prerequisite is that you have docker previously installed. 

For the installation of this project a script has been provided in another repository (https://gitlab.com/slavcho87/adidas). Anyway we leave the necessary commands for building and booting the docker images.

# Build and Run
In the repository https://gitlab.com/slavcho87/adidas are the instructions to install and run this project.

# Use

To launch calls to the API, you can be any rest client. I used Postman (https://www.getpostman.com/apps).

At the following address you can find a collection of postman
- /src/main/resources/adidas.postman_collection.json: postman collection for calls in the api.

# Technologies used

In this project the following technologies have been used:

- Java 1.8
- Flyway v8.5.11: continuous integration of the database
- spring boot v2.4.0
- swagger v2.9.2: framework to document rest api
- lombok v1.18.24: library that automatically plugs into your editor and build tools, spicing up your java.
- spring-kafka v2.8.6: apache kafka producer
