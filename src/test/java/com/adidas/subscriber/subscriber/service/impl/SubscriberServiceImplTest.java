package com.adidas.subscriber.subscriber.service.impl;

import com.adidas.subscriber.subscriber.domain.Subscriber;
import com.adidas.subscriber.subscriber.repository.SubscriberRepository;
import com.adidas.subscriber.subscriber.utils.PageableFactory;
import com.adidas.subscriber.subscriber.utils.PaginationFactoryTest;
import com.adidas.subscriber.subscriber.utils.SubscriberFactoryTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SubscriberServiceImplTest {

    @InjectMocks
    private SubscriberServiceImpl movieService;

    @Mock
    private SubscriberRepository repository;

    //SAVE

    @Test
    public void GivenSubscriberValid_WhenSave_ThenShouldBeSaved(){
        //GIVE
        Subscriber subscriber = SubscriberFactoryTest.createWithId1();
        when(repository.save(subscriber)).thenReturn(subscriber);

        //WHEN
        Subscriber savedSubscriber = movieService.save(subscriber);

        //THEN
        assertNotNull(savedSubscriber);
        assertEquals(subscriber.getId(), savedSubscriber.getId());
        assertEquals(subscriber.getEmail(), savedSubscriber.getEmail());
        assertEquals(subscriber.getFirstName(), savedSubscriber.getFirstName());
        assertEquals(subscriber.getGender(), savedSubscriber.getGender());
        assertEquals(subscriber.getBirthDate(), savedSubscriber.getBirthDate());
        assertEquals(subscriber.getConsentFlag(), savedSubscriber.getConsentFlag());
    }

    @Test(expected = NullPointerException.class)
    public void GivenSubscriberNull_WhenSave_ThenShoulThrowNullPointerException(){
        //GIVE
        Subscriber subscriber = null;


        //WHEN
        Subscriber savedSubscriber = movieService.save(subscriber);

        //THEN
        assertFalse(true); //If it reaches this point, the operation has not gone well.
    }

    //CANCEL

    @Test
    public void GivenSubscriberIdNotNull_WhenCancel_ThenShouldBeCanceled(){
        //GIVE
        Integer id = 1;

        //WHEN
        movieService.cancel(id);

        //THEN
        assertTrue(true); //If it reaches this point, the operation has gone well.
    }

    @Test(expected = NullPointerException.class)
    public void GivenSubscriberIdNull_WhenCancel_ThenShoulThrowNullPointerException(){
        //GIVE
        Integer id = null;

        //WHEN
        movieService.cancel(id);

        //THEN
        assertFalse(true); //If it reaches this point, the operation has not gone well.
    }

    //GET

    @Test
    public void GivenSubscriberIdNotNull_WhenGet_ThenValueShouldBeReturn(){
        //GIVE
        Integer id = 1;
        Subscriber subscriberToReturn = SubscriberFactoryTest.createWithId1();
        when(repository.findById(id)).thenReturn(Optional.of(subscriberToReturn));

        //WHEN
        Subscriber subscriber = movieService.get(id);

        //THEN
        assertNotNull(subscriber);
        assertEquals(subscriberToReturn.getId(), subscriber.getId());
        assertEquals(subscriberToReturn.getEmail(), subscriber.getEmail());
        assertEquals(subscriberToReturn.getFirstName(), subscriber.getFirstName());
        assertEquals(subscriberToReturn.getGender(), subscriber.getGender());
        assertEquals(subscriberToReturn.getBirthDate(), subscriber.getBirthDate());
        assertEquals(subscriberToReturn.getConsentFlag(), subscriber.getConsentFlag());
    }

    @Test(expected = NullPointerException.class)
    public void GivenSubscriberIdNull_WhenGet_ThenShoulThrowNullPointerException(){
        //GIVE
        Integer id = null;

        //WHEN
        Subscriber subscriber = movieService.get(id);

        //THEN
        assertFalse(true); //If it reaches this point, the operation has not gone well.
    }

    //GET ALL

    @Test(expected = NullPointerException.class)
    public void GivenPageNullParameter_WhenGetAll_ThenShoulThrowNullPointerException(){
        //GIVE
        final Integer page = null;
        final Integer pageSize = 10;
        final String sortBy = "id";
        final String sort = PageableFactory.ASC_SORT;

        //WHEN
        movieService.getAll(page, pageSize, sortBy, sort);

        //THEN
        assertFalse(true); ////If it reaches this point, the operation has not gone well.
    }

    @Test(expected = NullPointerException.class)
    public void GivenPageSizeNullParameter_WhenGetAll_ThenShoulThrowNullPointerException(){
        //GIVE
        final Integer page = 1;
        final Integer pageSize = null;
        final String sortBy = "id";
        final String sort = PageableFactory.ASC_SORT;

        //WHEN
        movieService.getAll(page, pageSize, sortBy, sort);

        //THEN
        assertFalse(true); ////If it reaches this point, the operation has not gone well.
    }

    @Test(expected = NullPointerException.class)
    public void GivenSortByNullParameter_WhenGetAll_ThenShoulThrowNullPointerException(){
        //GIVE
        final Integer page = 1;
        final Integer pageSize = 10;
        final String sortBy = null;
        final String sort = PageableFactory.ASC_SORT;

        //WHEN
        movieService.getAll(page, pageSize, sortBy, sort);

        //THEN
        assertFalse(true); ////If it reaches this point, the operation has not gone well.
    }

    @Test(expected = NullPointerException.class)
    public void GivenSortNullParameter_WhenGetAll_ThenShoulThrowNullPointerException(){
        //GIVE
        final Integer page = 1;
        final Integer pageSize = 10;
        final String sortBy = "id";
        final String sort = null;

        //WHEN
        movieService.getAll(page, pageSize, sortBy, sort);

        //THEN
        assertFalse(true); //If it reaches this point, the operation has not gone well.
    }

    @Test
    public void GivenAllParameter_WhenGetAll_ThenShoulThrowNullPointerException(){
        //GIVE
        final Integer page = 1;
        final Integer pageSize = 10;
        final String sortBy = "id";
        final String sort = PageableFactory.ASC_SORT;
        List<Subscriber> subscribers = SubscriberFactoryTest.createList();
        Page<Subscriber> result = PaginationFactoryTest.getPage(subscribers);

        when(repository.findAll((Pageable) any())).thenReturn(result);

        //WHEN
        List<Subscriber> resultPage = movieService.getAll(page, pageSize, sortBy, sort);

        //THEN
        assertNotNull(resultPage);
        assertArrayEquals(subscribers.toArray(), resultPage.toArray());
    }

}
