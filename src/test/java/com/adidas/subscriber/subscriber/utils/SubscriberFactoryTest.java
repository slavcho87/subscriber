package com.adidas.subscriber.subscriber.utils;


import com.adidas.subscriber.subscriber.domain.Subscriber;

import java.util.ArrayList;
import java.util.List;

public class SubscriberFactoryTest {
    public static Subscriber createWithId1(){
        Subscriber subscriber = new Subscriber();
        subscriber.setId(1);
        subscriber.setEmail("mario@test.com");
        subscriber.setFirstName("Mario");
        subscriber.setGender("M");
        subscriber.setBirthDate(DateUtilsTest.initDate(16, 10, 1987));
        subscriber.setConsentFlag(Boolean.TRUE);

        return subscriber;
    }

    public static Subscriber createWithId2(){
        Subscriber subscriber = new Subscriber();
        subscriber.setId(2);
        subscriber.setEmail("david@test.com");
        subscriber.setFirstName("david");
        subscriber.setGender("M");
        subscriber.setBirthDate(DateUtilsTest.initDate(10, 05, 1985));
        subscriber.setConsentFlag(Boolean.TRUE);

        return subscriber;
    }

    public static List<Subscriber> createList(){
        List<Subscriber> subscribers = new ArrayList();
        subscribers.add(SubscriberFactoryTest.createWithId1());
        subscribers.add(SubscriberFactoryTest.createWithId2());

        return subscribers;
    }




}
