package com.adidas.subscriber.subscriber.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtilsTest {

    public static Date initDate(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month+1);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        return calendar.getTime();
    }


}
