package com.adidas.subscriber.subscriber.utils;

import com.adidas.subscriber.subscriber.domain.Subscriber;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.util.StreamUtils;

import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

public class PaginationFactoryTest {
    public static Page<Subscriber> getPage(List<Subscriber> subscribers){
        return new Page<Subscriber>() {
            @Override
            public int getTotalPages() {
                return subscribers.size();
            }

            @Override
            public long getTotalElements() {
                return subscribers.size();
            }

            @Override
            public <U> Page<U> map(Function<? super Subscriber, ? extends U> converter) {
                return null;
            }

            @Override
            public int getNumber() {
                return subscribers.size();
            }

            @Override
            public int getSize() {
                return subscribers.size();
            }

            @Override
            public int getNumberOfElements() {
                return subscribers.size();
            }

            @Override
            public List<Subscriber> getContent() {
                return subscribers;
            }

            @Override
            public boolean hasContent() {
                return false;
            }

            @Override
            public Sort getSort() {
                return null;
            }

            @Override
            public boolean isFirst() {
                return false;
            }

            @Override
            public boolean isLast() {
                return false;
            }

            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }

            @Override
            public Pageable nextPageable() {
                return null;
            }

            @Override
            public Pageable previousPageable() {
                return null;
            }

            @Override
            public Iterator<Subscriber> iterator() {
                return null;
            }

            @Override
            public List<Subscriber> toList() {
                return subscribers;
            }
        };

    }


}
