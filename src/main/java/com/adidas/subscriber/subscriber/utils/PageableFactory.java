package com.adidas.subscriber.subscriber.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class PageableFactory {
    public static final String ASC_SORT = "ASC";
    public static final String DESC_SORT = "DESC";

    public static Pageable create(Integer page, Integer pageSize, String sortBy, String sort){
        Pageable pageable = null;

        if(ASC_SORT.equals(sort)){
            pageable = PageRequest.of((page.intValue() - 1), pageSize, Sort.by(sortBy).ascending());
        }else if(DESC_SORT.equals(sort)){
            pageable = PageRequest.of((page.intValue() - 1), pageSize, Sort.by(sortBy).descending());
        }else{
            pageable = PageRequest.of((page.intValue() - 1), pageSize, Sort.by(sortBy).descending());
        }

        return pageable;
    }
}
