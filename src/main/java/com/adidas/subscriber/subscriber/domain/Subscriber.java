package com.adidas.subscriber.subscriber.domain;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Getter
@Setter
public class Subscriber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "Email is mandatory")
    @Column
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column
    private String gender;

    @NotNull(message = "Birth date is mandatory")
    @Column(name = "birth_date")
    private Date birthDate;

    @NotNull(message = "Consent flag is mandatory")
    @Column(name = "consent_flag")
    private Boolean consentFlag;

}
