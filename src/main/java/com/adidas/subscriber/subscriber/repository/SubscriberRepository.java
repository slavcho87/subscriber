package com.adidas.subscriber.subscriber.repository;

import com.adidas.subscriber.subscriber.domain.Subscriber;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SubscriberRepository extends PagingAndSortingRepository<Subscriber, Integer> {
}
