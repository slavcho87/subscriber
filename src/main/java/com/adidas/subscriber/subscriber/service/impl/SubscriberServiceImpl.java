package com.adidas.subscriber.subscriber.service.impl;

import com.adidas.subscriber.subscriber.domain.Subscriber;
import com.adidas.subscriber.subscriber.repository.SubscriberRepository;
import com.adidas.subscriber.subscriber.service.SubscriberService;
import com.adidas.subscriber.subscriber.utils.PageableFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class SubscriberServiceImpl implements SubscriberService {

    @Autowired
    private SubscriberRepository repository;

    @Autowired
    private KafkaTemplate<String, Subscriber> kafkaTemplate;

    @Override
    public Subscriber save(Subscriber subscriber) {
        Assert.notNull(subscriber, SUBSCRIBER_NOT_NULL_MESSAGE);
        subscriber = this.repository.save(subscriber);

        kafkaTemplate.send("sendEmail", subscriber);

        return subscriber;
    }

    @Override
    public void cancel(Integer id) {
        Assert.notNull(id, SUBSCRIBER_ID_NOT_NULL_MESSAGE);
        this.repository.deleteById(id);
    }

    @Override
    public Subscriber get(Integer id) {
        Assert.notNull(id, SUBSCRIBER_ID_NOT_NULL_MESSAGE);
        return repository.findById(id).get();
    }

    @Override
    public List<Subscriber> getAll(Integer page, Integer pageSize, String sortBy, String sort) {
        Assert.notNull(page, PAGE_NOT_NULL_MESSAGE);
        Assert.notNull(pageSize, PAGE_SIZE_NOT_NULL_MESSAGE);
        Assert.notNull(sortBy, SORT_BY_NOT_NULL_MESSAGE);
        Assert.notNull(sort, SORT_NOT_NULL_MESSAGE);

        Pageable pageable = PageableFactory.create(page, pageSize, sortBy, sort);
        return this.repository.findAll(pageable).toList();
    }

}
