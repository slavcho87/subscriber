package com.adidas.subscriber.subscriber.service;

import com.adidas.subscriber.subscriber.domain.Subscriber;

import java.util.List;

public interface SubscriberService {

    public final static String SUBSCRIBER_NOT_NULL_MESSAGE = "Subscriber should be non-null";
    public final static String SUBSCRIBER_ID_NOT_NULL_MESSAGE = "Subscriber id should be non-null";
    public final static String PAGE_NOT_NULL_MESSAGE = "Page should be non-null";
    public final static String PAGE_SIZE_NOT_NULL_MESSAGE = "PageSize should be non-null";
    public final static String SORT_BY_NOT_NULL_MESSAGE = "SortBy should be non-null";
    public final static String SORT_NOT_NULL_MESSAGE = "Sort should be non-null";

    public Subscriber save(Subscriber subscriber);

    public void cancel(Integer id);

    public Subscriber get(Integer id);

    public List<Subscriber> getAll( Integer page, Integer pageSize, String sortBy, String sort);

}
