package com.adidas.subscriber.subscriber.controller;

import com.adidas.subscriber.subscriber.controller.dto.SubscriberDTO;
import com.adidas.subscriber.subscriber.domain.Subscriber;
import com.adidas.subscriber.subscriber.service.SubscriberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController()
@Api(value="Subscriber API REST")
public class SubscriberController {

    @Autowired
    private SubscriberService subscriberService;

    @Autowired
    private ModelMapper modelMapper;

    @ApiOperation(value = "Create new subscription", response = SubscriberDTO.class)
    @RequestMapping(value = "/api/public/subscriber", method = RequestMethod.POST)
    public ResponseEntity save(@RequestBody SubscriberDTO subscriberDTO){
        Subscriber subscriber = modelMapper.map(subscriberDTO, Subscriber.class);
        subscriber = subscriberService.save(subscriber);

        return ResponseEntity.ok(modelMapper.map(subscriber, SubscriberDTO.class));
    }

    @ApiOperation(value = "Cancel exist subscription")
    @ResponseStatus(code = HttpStatus.OK)
    @RequestMapping(value = "/api/private/subscriber/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('ROLE_BACK_OFFICE')")
    public void cancel(@PathVariable Integer id){
        subscriberService.cancel(id);
    }

    @ApiOperation(value = "Get exist subscription")
    @RequestMapping(value = "/api/private/subscriber/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_BACK_OFFICE')")
    public ResponseEntity get(@PathVariable Integer id){
        Subscriber subscriber  = subscriberService.get(id);
        return ResponseEntity.ok(modelMapper.map(subscriber, SubscriberDTO.class));
    }

    @ApiOperation(value = "Get all exist subscriptions")
    @RequestMapping(value = "/api/private/subscriber", params = {"page", "pageSize", "sortBy", "sort"}, method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_BACK_OFFICE')")
    public ResponseEntity getAll(@RequestParam Integer page, @RequestParam Integer pageSize, @RequestParam String sortBy, @RequestParam String sort){
        List<SubscriberDTO> subscribers = subscriberService.getAll(page, pageSize, sortBy, sort).stream()
                .map(subscriber -> modelMapper.map(subscriber, SubscriberDTO.class))
                .collect(Collectors.toList());
        return ResponseEntity.ok(subscribers);
    }

}
