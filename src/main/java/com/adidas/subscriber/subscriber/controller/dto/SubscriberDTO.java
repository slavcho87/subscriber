package com.adidas.subscriber.subscriber.controller.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class SubscriberDTO {

    private Integer id;

    private String email;

    private String firstName;

    private String gender;

    private Date birthDate;

    private Boolean consentFlag;

}
