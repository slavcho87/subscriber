package com.adidas.subscriber.subscriber.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.NoSuchElementException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class SubscriberRestException extends ResponseEntityExceptionHandler {

    @ExceptionHandler(PropertyReferenceException.class)
    protected ResponseEntity<ApiException> propertyReferenceExceptoionException(PropertyReferenceException ex){
        ApiException apiError = new ApiException();
        apiError.setStatus(HttpStatus.BAD_REQUEST);
        apiError.setTimestamp(new Date().getTime());
        apiError.setMessage(ex.getMessage());

        return new ResponseEntity(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    protected ResponseEntity<ApiException> emptyResultDataAccessException(EmptyResultDataAccessException ex){
        ApiException apiError = new ApiException();
        apiError.setStatus(HttpStatus.NOT_FOUND);
        apiError.setTimestamp(new Date().getTime());
        apiError.setMessage(ex.getMessage());

        return new ResponseEntity(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NoSuchElementException.class)
    protected ResponseEntity<ApiException> noSuchElementException(NoSuchElementException ex){
        ApiException apiError = new ApiException();
        apiError.setStatus(HttpStatus.NOT_FOUND);
        apiError.setTimestamp(new Date().getTime());
        apiError.setMessage(ex.getMessage());

        return new ResponseEntity(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseEntity<ApiException> illegalArgumentException(IllegalArgumentException ex){
        ApiException apiError = new ApiException();
        apiError.setStatus(HttpStatus.BAD_REQUEST);
        apiError.setTimestamp(new Date().getTime());
        apiError.setMessage(ex.getMessage());

        return new ResponseEntity(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<ApiException> validationException(ConstraintViolationException ex){
        ApiException apiError = new ApiException();
        apiError.setStatus(HttpStatus.BAD_REQUEST);
        apiError.setTimestamp(new Date().getTime());
        apiError.setMessage(ex.getMessage());

        return new ResponseEntity(apiError, HttpStatus.BAD_REQUEST);
    }

}
