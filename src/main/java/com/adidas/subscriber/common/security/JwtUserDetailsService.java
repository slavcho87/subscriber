package com.adidas.subscriber.common.security;

import com.adidas.subscriber.user.domain.User;
import com.adidas.subscriber.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getByName(username);

        if(user == null){
            throw new UsernameNotFoundException("User/password not valid");
        }

        return new org.springframework.security.core.userdetails.User(
                user.getName(),
                user.getPass(),
                AuthorityUtils.createAuthorityList(user.getRole())
        );
    }
}