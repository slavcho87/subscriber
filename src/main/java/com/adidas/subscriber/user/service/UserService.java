package com.adidas.subscriber.user.service;

import com.adidas.subscriber.user.domain.User;


public interface UserService {

    public final static String USERNAME_NOT_NULL_MESSAGE = "Username should be non-null";
    public final static String PASSWORD_NOT_NULL_MESSAGE = "Password should be non-null";

    public static final String BACKEND_ROLE = "ROLE_BACKEND_ROLE";

    public User getByName(String name);

    public String login(String userName, String password) throws Exception;
}
