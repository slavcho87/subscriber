package com.adidas.subscriber.user.service.impl;

import com.adidas.subscriber.user.domain.User;
import com.adidas.subscriber.user.repository.UserRepository;
import com.adidas.subscriber.user.service.UserService;
import com.adidas.subscriber.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public User getByName(String name) {
        Assert.notNull(name, USERNAME_NOT_NULL_MESSAGE);
        return this.userRepository.findByName(name);
    }

    @Override
    public String login(String userName, String password) throws Exception {
        Assert.notNull(userName, USERNAME_NOT_NULL_MESSAGE);
        Assert.notNull(password, PASSWORD_NOT_NULL_MESSAGE);

        User user = this.getByName(userName);
        Assert.notNull(user, "User/password not valid");

        checkPassword(password, user.getPass());
        authenticate(user.getName(), password);

        return jwtUtils.generate(user);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    private void checkPassword(String password, String encriptPass) throws Exception {
        if(!BCrypt.checkpw(password, encriptPass)){
            throw new Exception("Password incorrect");
        }
    }

}
