package com.adidas.subscriber.user.utils;

import com.adidas.subscriber.user.domain.User;

public class UserFactory {
    public static User create(Integer id, String name, String  role){
        User user = new User();
        user.setId(id);
        user.setName(name);
        user.setRole(role);

        return user;
    }
}
