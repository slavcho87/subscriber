package com.adidas.subscriber.user.controller;

import com.adidas.subscriber.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.adidas.subscriber.user.controller.dto.UserDTO;

@RestController()
@Api(value="User API REST")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @ApiOperation(value = "Login", response = String.class)
    @RequestMapping(value = "/api/public/login", method = RequestMethod.POST)
    public ResponseEntity login(@RequestBody UserDTO userDTO) throws Exception {
            String jwt = userService.login(userDTO.getUser(), userDTO.getPassword());
            return ResponseEntity.ok(jwt);
    }

}
