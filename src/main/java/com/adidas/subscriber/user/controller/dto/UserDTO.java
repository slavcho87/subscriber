package com.adidas.subscriber.user.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO {

    private String user;

    private String password;

}
