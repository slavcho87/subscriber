package com.adidas.subscriber.user.repository;

import com.adidas.subscriber.user.domain.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User, Integer>  {
    User findByName(String name);
}
