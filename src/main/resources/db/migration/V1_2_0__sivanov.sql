CREATE TABLE Users (
    id bigint PRIMARY KEY auto_increment,
    name VARCHAR(30),
    pass VARCHAR(255),
    role VARCHAR(255)
);

INSERT INTO Users (id, name, pass, role)
VALUES (1l, 'David', '$2a$10$fwj4jzJw3rFJpO6KpTUBC.hFDlZhHLl0S9RujbP3X1H7RKN/p6FQq', 'ROLE_BACK_OFFICE');
