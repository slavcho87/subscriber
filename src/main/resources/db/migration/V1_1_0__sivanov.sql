CREATE TABLE Subscriber (
    id bigint PRIMARY KEY auto_increment,
    email varchar(50),
    first_name varchar(50) NULL,
    gender varchar(1) NULL,
    birth_date datetime,
    consent_flag boolean
);

create INDEX subscriber_id_idx on Subscriber(id);
create INDEX subscriber_email_idx on Subscriber(email);